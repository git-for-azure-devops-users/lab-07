# Git for TFS users
Lab 07: Working with shared feature branches

---

# Tasks

 - Create a common feature branch
 
 - Create and work in a user branch based on the feature branch
 
 - Merge your user branch into feature from visual studio
  
 - Merge the feature branch into master

---

## Preparations

 - Agree with someone else to work together
 
 - Decide who will create the feature branch for both

---

## Create a common feature branch

 - Clone the lab repository from visual studio:
```
http://<server-ip>:8080/tfs/DefaultCollection/git-workshop/_git/demo-app-lab-07
```

 - Create a feature branch "feature/user1-user2" based on master (one branch for each group)

---

## Create and work in a user branch based on the feature branch

 - Fetch the repository and checkout the feature branch

&nbsp;
<img alt="Image 1.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/pulling/fetch.png?view=azure-devops" border="1">
&nbsp;

 - Perform some changes, commit and push them to the remote repository

---

## Merge your user branch into feature from visual studio

 - Checkout the feature branch
  
 - From the "Branches" view merge your user branch into the feature branch

&nbsp;
<img alt="Image 1.1" src="https://metavrse.files.wordpress.com/2018/02/fill-source-branch-name-and-click-merge.png?w=1100" border="1">
&nbsp;

- Then push your changes to the remote repository

---

## Merge the feature branch into master

 - From the web portal create a pull request from your user branch to your feature branch

&nbsp;
<img alt="Image 1.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/pull-request-detail.png?view=azure-devops" border="1">
&nbsp;

 - Set the other user as "code reviewer" and create the pull request
 
 - Complete the pull request to merge your changes
